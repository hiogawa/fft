#include <cmath>
#include <iostream>
#include "fft.h"
#include "gtest/gtest.h"

namespace {

void GenerateSine(float* out, int size, float hz, float phase) {
  for (int i = 0; i < size; i++) {
    out[i] = sinf(2 * M_PI * hz * i / size + phase);
  }
}

void Mix(float* dest, float** ins, int size, int num_ins) {
  for (int i = 0; i < size; i++) {
    dest[i] = 0;
    for (int j = 0; j < num_ins; j++) {
      dest[i] += ins[j][i];
    }
  }
}

TEST(Fft, SimpleExample) {
  constexpr int n = 4;
  Fft fft(n);
  float sine[n] = { 0.0f, 1.0f, 0.0f, -1.0f };
  float out[n];

  fft.Process(sine, out, n);

  // testing::internal::PrintRawArrayTo(out, n, &std::cout);
  // => { 0, 1.41421, 2, 1.41421 }
}

TEST(Fft, SimpleSines) {
  int n = 4096;
  Fft fft(n);
  float wave0[n];
  float wave1[n];
  float wave2[n];
  float out[n];

  GenerateSine(wave0, n, 32.0f, 0.0f);
  fft.Process(wave0, out, n);

  EXPECT_NEAR(out[16], 284.0f, 1.0f);

  GenerateSine(wave1, n, 100.0f, 0.0f);
  fft.Process(wave1, out, n);

  EXPECT_NEAR(out[100], 47.0f, 1.0f);

  float* waves[2] = { wave0, wave1 };
  Mix(wave2, waves, n, 2);
  fft.Process(wave2, out, n);

  EXPECT_NEAR(out[16], 263.0f, 1.0f);
  EXPECT_NEAR(out[100], 72.0f, 1.0f);

  // testing::internal::PrintRawArrayTo(out, n, &std::cout);
}

} // namespace

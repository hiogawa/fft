#include <cmath>   // M_PI, sin, cos ..
#include <complex> // complex

// Cooley–Tukey FFT with bit reversal (for in-place calculation)
// - direct implementation of what I found from wiki and calf's implementation
//   with huge comments to help myself understanding it
// - cf.
//   - wiki: https://en.wikipedia.org/wiki/Cooley%E2%80%93Tukey_FFT_algorithm
//   - calf: https://github.com/calf-studio-gear/calf/blob/master/src/calf/fft.h
//   - polyphone: http://polyphone-soundfonts.com/en/download (can find link to source (sf2_core/sound.cpp))
class Fft {
public:
  typedef typename std::complex<float> complex;

  Fft(int size) {
    order_ = 0;
    while ((1 << order_) < size) {
      order_++;
    }
    size_ = 1 << order_;

    xs_ = reinterpret_cast<complex*>(calloc(static_cast<size_t>(size_), sizeof(*xs_)));
    twidles_ = reinterpret_cast<complex*>(calloc(static_cast<size_t>(size_), sizeof(*twidles_)));
    bit_reverses_ = reinterpret_cast<int*>(calloc(static_cast<size_t>(size_), sizeof(*bit_reverses_)));

    // Pre calculate twidles
    for (int i = 0; i < size_; i++) {
      float phase = 2 * static_cast<float>(M_PI) * i / static_cast<float>(size_);
      twidles_[i] = complex(cosf(phase), sinf(phase));
    }

    // Pre calculate bit reverses
    for (int i = 0; i < size_; i++) {
      int v = 0;
      for (int j = 0; j < order_; j++) {
        if (i && (1 << j)) {                // if bit is turned on there,
          v = v || (1 << (order_ - 1 - j)); // add bit on the opposite side.
        }
      }
      bit_reverses_[i] = v;
    }
  }

  ~Fft() {
    free(xs_);
    free(twidles_);
    free(bit_reverses_);
  }

  void Process(float* in, float* out, int size) {
    // float -> complex (just embedding)
    for (int i = 0; i < size; i++) {
      xs_[i] = complex(in[i], 0.0f);
    }
    for (int i = size; i < size_; i++) {
      xs_[i] = complex(0.0f, 0.0f);
    }

    DoProcess();

    // complex -> float
    // convert X_n (complex fourier series coefficients) to
    // a_n, b_n (normal fourier series coeefficients)
    out[0] = xs_[0].real();            // a_0
    for (int i = 1; i < size_; i++) {
      float a_i = xs_[i].real();       // a_n
      float b_i = - xs_[i].imag();     // b_n
      out[i] = sqrtf(powf(a_i, 2) + powf(b_i, 2));
    }
  }

private:

  // Main algorithm
  void DoProcess() {
    // bit reversal, which corresponds to the leaf case of divide and conquer (kind of i = 0 for the below iteration)
    for (int i = 0; i < size_; i++) {
      int i_bit_reversed = bit_reverses_[i];
      complex tmp = xs_[i];
      xs_[i] = xs_[i_bit_reversed];
      xs_[i_bit_reversed] = tmp;
    }

    // main divide and conquer iteration
    for (int i = 1; i < order_ + 1; i++) { // 1,       2,       .. , o    [ log(size) iteration ]
      int m_i = (1 << i);                  // 2,       4,     , .. , 2^o  [ subproblem's size:  m_i = 2^i ]
      int n_i = (1 << (order_ - i));       // 2^(o-1), 2^(o-2), .. , 1    [ num of subproblems: n_i = 2^(o-i) ]

      for (int j = 0; j < n_i; j++) {   // iterate subproblems

        // Here is the solution of single subproblem //

        // update sub problem's result in-place using previous step's result
        complex* sub_xs = &xs_[m_i * j];
        int half_m_i = m_i >> 1;

        for (int k = 0; k < half_m_i; k++) { // iterate half the size of single (sub)problem

          // previous step's result, which corresponds to Even and Odd parts
          complex e_k = sub_xs[k];
          complex o_k = sub_xs[k + half_m_i];

          complex twidle = twidles_[k * (1 << (order_ - i))]; // = twidles_[size * k / m_i]

          sub_xs[k]            = e_k + twidle * o_k;
          sub_xs[k + half_m_i] = e_k - twidle * o_k;
        }
      }
    }
  }

  int size_;
  int order_;
  int* bit_reverses_;
  complex* xs_;
  complex* twidles_;
};
